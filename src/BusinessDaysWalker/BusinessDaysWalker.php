<?php
namespace BusinessDaysWalker;

use DateTime;

class BusinessDaysWalker
{
    private $date;
    private $calendar;

    /**
     *
     * @param HolidayCalendarProvider $calendar
     * @param \DateTime $date
     * @param string $goto_business_day
     */
    public function __construct(HolidayCalendarProvider $calendar, DateTime $date = null, $goto_business_day = false)
    {
        if ($date === null) {
            $date = new DateTime('now');
        } else {
            $date = clone $date;
        }

        $this->date = $date;
        $this->calendar = $calendar;

        if ($goto_business_day && !$this->calendar->isWorkingDay($this->date)) {
            $this->goToBusinessDay();
        }
    }

    /**
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     *
     * @param number $days
     * @return \BusinessDaysWalker\BusinessDaysWalker
     */
    public function next($days = 1)
    {
        while ($days--) {
            $this->goToBusinessDay();
        }
        return $this;
    }

    /**
     *
     * @param number $days
     * @return \BusinessDaysWalker\BusinessDaysWalker
     */
    public function prev($days = 1)
    {
        while ($days--) {
            $this->goToBusinessDay(true);
        }
        return $this;
    }

    /**
     *
     * @param bool $reverse
     * @return \BusinessDaysWalker\BusinessDaysWalker
     */
    public function goToBusinessDay($reverse = false)
    {
        do {
            $this->date->modify(sprintf('%s1 day', $reverse ? '-' : '+'));
        } while (!$this->calendar->isWorkingDay($this->date));

        return $this;
    }
}
