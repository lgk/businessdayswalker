<?php
namespace BusinessDaysWalker;

use DateTime;

interface HolidayCalendarProvider
{
    /**
     * @param DateTime $date
     * @return bool
     */
    public function isHolidayDay(DateTime $date);

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isWeekend(DateTime $date);

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isWorkingDay(DateTime $date);
}
