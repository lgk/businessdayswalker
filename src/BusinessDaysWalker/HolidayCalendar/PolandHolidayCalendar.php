<?php
namespace BusinessDaysWalker\HolidayCalendar;

use DateTime;
use BusinessDaysWalker\HolidayCalendarProvider;

class PolandHolidayCalendar implements HolidayCalendarProvider
{
    public function isHolidayDay(DateTime $date)
    {
        $year = intval(date('Y', $date->getTimestamp()));
        $easter_day = easter_date($year);

        $holidays = array(
            mktime(0, 0, 0, 1, 1, $year), // nowy rok
            mktime(0, 0, 0, 1, 6, $year), // trzech kroli
            $easter_day, // pierwszy dzien wielkiej nocy
            $easter_day + 86400, // drugi dzien wielkiej nocy
            mktime(0, 0, 0, 5, 1, $year), // 1 maja
            mktime(0, 0, 0, 5, 3, $year), // 3 maja
            $easter_day + (60 * 86400), // boze cialo
            $easter_day + (49 * 86400), // zielone swiatki
            mktime(0, 0, 0, 8, 15, $year), // 15 sierpnia
            mktime(0, 0, 0, 11, 1, $year), // 1 listopada
            mktime(0, 0, 0, 11, 11, $year), // 11 listopada
            mktime(0, 0, 0, 12, 25, $year), // 25 grudnia
            mktime(0, 0, 0, 12, 26, $year) // 26 grudnia
        );

        return in_array(strtotime('midnight', $date->getTimestamp()), $holidays);
    }

    public function isWeekend(DateTime $date)
    {
        $dayOfWeek =  intval(date('w', $date->getTimestamp()));
        return $dayOfWeek == 0 || $dayOfWeek == 6;
    }

    public function isWorkingDay(DateTime $date)
    {
        return $this->isHolidayDay($date) === false && $this->isWeekend($date) === false;
    }
}
