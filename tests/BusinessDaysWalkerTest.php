<?php
class BusinessDaysWalkerTest extends PHPUnit_Framework_TestCase
{
    private $calendar;

    public function setUp()
    {
        $this->calendar = new \BusinessDaysWalker\HolidayCalendar\PolandHolidayCalendar;
    }

    public function testGetDateInstance()
    {
        $walker = new \BusinessDaysWalker\BusinessDaysWalker($this->calendar, new \DateTime('now'));
        $this->assertInstanceOf('DateTime', $walker->getDate());
    }

    public function testNext()
    {
        $date1 = self::createDate('01-01-2015');
        $date2 = self::createDate('02-01-2015');

        $walker = new \BusinessDaysWalker\BusinessDaysWalker($this->calendar, $date1);
        $this->assertEquals($walker->next()->getDate(), $date2);
    }

    public function testAutoGotoBusinessDay()
    {
        $date= self::createDate('01-02-2015');
        $walker = new \BusinessDaysWalker\BusinessDaysWalker($this->calendar, $date, true);
        $this->assertEquals('02-02-2015', $walker->getDate()->format('d-m-Y'));
    }

    public function testPrev()
    {
        $date1 = self::createDate('01-02-2015');
        $date2 = self::createDate('30-01-2015');

        $walker = new \BusinessDaysWalker\BusinessDaysWalker($this->calendar, $date1);
        $this->assertEquals($walker->prev()->getDate(), $date2);
    }

    public function test10BusinessDays()
    {
        $walker = new \BusinessDaysWalker\BusinessDaysWalker($this->calendar, self::createDate('05-03-2015')->modify('midnight'), true);
        $this->assertEquals($walker->next(10)->getDate()->modify('midnight'), self::createDate('19-03-2015')->modify('midnight'));

        $walker = new \BusinessDaysWalker\BusinessDaysWalker($this->calendar, self::createDate('19-03-2015')->modify('midnight'), true);
        $this->assertEquals($walker->prev(10)->getDate()->modify('midnight'), self::createDate('05-03-2015')->modify('midnight'));
    }

    private static function createDate($date_str)
    {
        return \DateTime::createFromFormat('d-m-Y', $date_str);
    }
}
